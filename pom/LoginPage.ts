import { type Locator, type Page } from "@playwright/test";

export class LoginPage{
    readonly page: Page;
    readonly inputUsername: Locator;
    readonly inputPassword: Locator;
    readonly loginButton: Locator;

    constructor(page: Page){
        this.page = page;
        this.inputUsername = page.getByLabel('Username');
        this.inputPassword = page.getByLabel('Password');
        this.loginButton = page.getByRole('button', {name: 'Login'});
    }

    async navigateToWebPage() {
      await this.page.goto('https://katalon-demo-cura.herokuapp.com');
    }
    
    async navigateToLoginPage() {
      await this.page.getByRole('link', { name: 'Make Appointment' }).click();
    }
  
    async enterUsername(username: string) {
      await this.inputUsername.fill(username);
    }
  
    async enterPassword(password: string) {
      await this.inputPassword.fill(password);
    }
  
    async clickLogin() {
      await this.loginButton.click();
    }
  
    async login(username: string, password: string) {
      await this.enterUsername(username);
      await this.enterPassword(password);
      await this.clickLogin();
    }
}