import {test, expect} from "@playwright/test";
import { qase } from "playwright-qase-reporter";
import { LoginPage } from "../pom/LoginPage";
import { AppointmentPage } from "../pom/AppointmentPage";
import loginData from "../data/login.json";
import AppointData from "../data/appointment.json";

let webpage: LoginPage;

test.beforeEach(async ({ page }) => {
    await page.goto('https://katalon-demo-cura.herokuapp.com');
    webpage = new LoginPage(page);
});

test.describe('Login Testsuite', ()=>{

    test('Login with valid credentials', async({page}) => {
        qase.id(1);
        qase.title('Login with valid credentials');
        qase.fields({'Type':'Functional', 'Behavior':'Positive', 'Severity':'Normal', 'Priority':'Menium'});

        await test.step('Click Make Appointment', async()=>{
            await page.getByRole('link', { name: 'Make Appointment' }).click();
        });

        await test.step('Login dengan valid credentials', async()=>{
            await webpage.login(loginData[0].username, loginData[0].password);
        });
        
        await test.step('Verifikasi apakah object h1 Make Appointment ada di tampilan', async()=>{
            await expect(page.getByRole('heading', { name: 'Make Appointment' })).toBeVisible();
        });
        
    });

    test('Login with invalid credentials', async({page}) => {
        qase.id(2);
        qase.title('Login with invalid credentials');
        qase.fields({'Type':'Functional', 'Behavior':'Negative', 'Severity':'Normal', 'Priority':'Menium'});

        await test.step('Click Make Appointment', async()=>{
            await page.getByRole('link', { name: 'Make Appointment' }).click();
        });

        await test.step('Login dengan invalid credentials', async()=>{
            await webpage.login(loginData[1].username, loginData[1].password);
        });
        
        await test.step('Verifikasi apakah validasi username atau passsword muncul', async()=>{
            await expect(page.getByText('Login failed! Please ensure the username and password are valid.')).toBeVisible();
        });
        
    });
});

test.describe('Appointment Testsuite', ()=>{
    let appoinmentWebPage: AppointmentPage;

    test.beforeEach(async ({page})=>{
        appoinmentWebPage = new AppointmentPage(page);

        await test.step("Login", async()=>{
            await page.getByRole('link', { name: 'Make Appointment' }).click();
            await webpage.login(loginData[0].username, loginData[0].password);
        });
        
        await page.waitForURL('**/#appointment');
    });

    test('Success make appoinment', async({page}) => {
        qase.id(3);
        qase.title('Success make appoinment');
        qase.fields({'Type':'Functional', 'Behavior':'Positive', 'Severity':'Normal', 'Priority':'Menium'});

        await test.step('Submit Appointment', async()=>{
            await appoinmentWebPage.submitAppointment(
                AppointData[0].facility,
                AppointData[0].healthcare_program,
                AppointData[0].visit_date,
                AppointData[0].comment);
    
            await page.waitForURL('**/appointment.php#summary');
        });
        

        await test.step('Verifikasi Appointment Confirmation ada di tampilan', async()=>{
            await expect(page.getByRole('heading', { name: 'Appointment Confirmation' })).toBeVisible();
        });
    });

    test('Make an appoinment without input visit date', async({page}) => {
        qase.id(4);
        qase.title('Make an appoinment without input visit date');
        qase.fields({'Type':'Functional', 'Behavior':'Negative', 'Severity':'Normal', 'Priority':'Menium'});

        await test.step('Submit Appointment', async()=>{
            await appoinmentWebPage.submitAppointment(
                AppointData[1].facility,
                AppointData[1].healthcare_program,
                AppointData[1].visit_date,
                AppointData[1].comment);
        });
        

        await test.step('Verifikasi Appointment Confirmation tidak ada di tampilan', async()=>{
            await expect(page.getByRole('heading', { name: 'Appointment Confirmation' })).toBeHidden();
        });
    });
});