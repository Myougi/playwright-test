import { test, expect } from '@playwright/test';

let token;

test('Get Token Indomaret', async ({ request }) => {
    const newReq = await request.post(`https://dev-payment.kreditplus.id/api/authentication/v1/get-token`, {
      data: {
        client_key: 'zF2sVRKSZdZpCGD5/pJWDWQKu+EFU3y7ghlCsZBsqhU='
      }
    });
    expect(newReq.status()).toBe(200);

    const responseBody = await newReq.json();
    token = responseBody.token
});

test('Get Inquiry Indomaret', async ({ request }) => {
    const newReq = await request.post(`https://dev-payment.kreditplus.id/api/authentication/v1/get-token`, {
      data: {
        client_key: 'zF2sVRKSZdZpCGD5/pJWDWQKu+EFU3y7ghlCsZBsqhU='
      }
    });
    expect(newReq.status()).toBe(200);

    const responseBody = await newReq.json();
    token = responseBody.token

    const newReq2 = await request.post(`https://dev-payment.kreditplus.id/api/authentication/v1/get-token`, {
        headers:{
            'Authorization': `Bearer ${token}`
        },
        data: {
          client_key: 'zF2sVRKSZdZpCGD5/pJWDWQKu+EFU3y7ghlCsZBsqhU='
        }
      });
});